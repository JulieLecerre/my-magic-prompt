#!/bin/bash

helpList () {
  echo "ls : Allows you to list available files and folders that are visible and hidden"
  echo "rm : Allows you to delete a file"
  echo "rmd : Allows you to delete a folder"
  echo "version ou --v ou vers : Allows you to view the version of your prompt"
  echo "age : Lets you know if you are a minor or an adult"
  echo "quit : Allows you to get out of prompt"
  echo "profile : Allows you to display your surname, first name, age and email address"
  echo "passw : Allows you to change the password"
  echo "cd : Allows you to go to the last folder created or to return to the previous folder"
  echo "pwd : Allows you to specify the directory in which you are"
  echo "hour : Allows you to specify the time"
  echo "httpget : Allows you to download the html source code of a web page and save it to a specific file"
}

version()
{
  echo "this is the version 1.0"
}

profile() {
  echo "Julie"
  echo "Lecerre"
  echo "30"
  echo "julie.lecerre@gmail.com"
}

age() {
  echo -n "How old are you? : "
  read age
    if [[ $((age)) -lt 18 ]]; then
  echo "You are minor"
    fi
  echo "Your are major";
}

hour() {
  now=$(date +"%T")
  echo "Current time : $now"
}

passw() {
  echo -n "New password: "
  read passwordUn
  echo -n "Confirm your password: "
  read passwordDeux
if [[ $((passwordUn)) -eq $((passwordDeux)) ]]; then
  echo "Your password is changed"
  exit 
fi 
  echo "Please enter the same password"
}

httpGet() {
  wget http://google.com mapage.txt
}

list()
{
  ls -la
}

open() {
  vim  $arg
}

rmFile() {
  if test -f $arg; then
    rm $arg
  else
    echo "This is not a file or the file does not exist"
    exit
  fi  
}

rmDirectory() {
  if test -d $arg; then
    rm -R $arg
  else
    echo "This is not a directory or the directory does not exist"
    exit
  fi
}

cd2 () {
  cd $arg
}

pwd2 () {
    pwd $arg
}

main () { 
    echo -n "Login : "
    read login
    echo -n "Password : "
    read -s password

    echo ""
    echo ${login} ${password}
    if [ "$login" != "julie" ] || [ "$password" != "toto" ]; then
      echo "Invalid login or password"
      exit
    else
      echo "valide";
      while true 
      do
      read -p 'type command : ' cmd arg
        case "$cmd" in
          version | --v | vers ) version;;
          help ) helpList;;
          about ) about;;
          quit ) exit;;
          profile ) profile;;
          age ) age;;
          hour ) hour;;
          passw ) passw;;
          httpget ) httpGet;;
          ls ) list;;
          rm ) rmFile $arg;;
          rmd | rmdir ) rmDirectory $arg;;
          smtp ) smtp;;
          cd ) cd2 $arg;;
          pwd ) pwd2 $arg;;
          open ) open $arg;;
          * ) echo "Command Invalid";;
        esac
      done
    fi
}

main