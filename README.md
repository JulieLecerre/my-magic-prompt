### My-magic-prompt - Julie Lecerre - Paris
### Presentation

This prompt allows the user to enter a secure space through login (password and login). Many commands allow the user to change his password, delete files or folders, list his documents, or download the html source code of a web page.

### Links

`<link>` : <https://gitlab.com>

#### Package install

ssmtp
                   
### Orders
                    
Order  | Function
------------- | -------------
ls | Allows you to list available files and folders that are visible and hidden
rm | Allows you to delete a file 
rmd  | Allows you to delete a folder
version ou --v ou vers  |  Allows you to view the version of your prompt
age | Lets you know if you are a minor or an adult
quit | Allows you to get out of prompt
profile | Allows you to display your surname, first name, age and email address
passw | Allows you to change the password
cd | Allows you to go to the last folder created or to return to the previous folder
pwd |  Allows you to specify the directory in which you are
hour | Allows you to specify the time
httpget | Allows you to download the html source code of a web page and save it to a specific file

### End